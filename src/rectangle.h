#include "shape.h"

class Rectangle: public Shape {	//RECTANGLE IS A CHILD OF SHIP
	public:
		Rectangle(int a= 0, int b= 0): Shape(a,b) {} //CALL CONSTRUCTOR OF PARENT

		int area() {
			printf("\n Rectangle class area: ");
			return (width*height);
		}
};
