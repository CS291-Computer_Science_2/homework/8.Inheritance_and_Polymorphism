#pragma once

#include <iostream>
using namespace std;

class Shape {
	protected: //ALLOWS ACCESS TO ONLY THESE METHODS: self, children, friends.
		int width, height;

	public:
		Shape(int a=0, int b=0): width(move(a)), height(move(b)) {}
		~Shape() {}

		/*PURE VIRTUAL, ABSTRACT FUNCTION*/
		/*EVERY CHILD MUST DEFINE THE FUNCTION*/
		virtual	int area()=0;

		
//	 ### Dynamically check call at runtime (for children of class) ###	
		/*virtual int area(){
				cout<<"something";
				int dosomething;
				return dosomething;
		}*/		

};
